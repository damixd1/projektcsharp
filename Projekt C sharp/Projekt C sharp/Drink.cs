﻿using System;

namespace Projekt_C_sharp
{
    public enum IsFizzy
    {
        Tak,
        Nie,
    };

    public class Drink : Product
    {
        public string brand { get; set; }
        public IsFizzy fizzy { get; set; }

        public Drink(string brand, IsFizzy fizzy, double price, double weight, Promotion discount)
        {
            this.price = price;
            this.weight = weight;
            this.fizzy = fizzy;
            this.brand = brand;
            this.discount = discount;

            this.priceAfterPromotion = Ratio();
        }

        protected override string Ratio()
        {
            string ratio;
            try
            {
                if (price / (int)discount < price)
                {
                    ratio = discount.ToString() + " " + Math.Round(((price / (int)discount) / weight), 2) + " za Litr";
                }
                else
                {
                    ratio = "Brak promocji";
                }
            }
            catch (DivideByZeroException e)
            {
                ratio = "Błąd";
            }

            return ratio;
        }
    }
}
