﻿using System;

namespace Projekt_C_sharp
{
    public class Fruit : Product
    {
        public string countryOfOrigin { get; set; } = "nieznany";
        public string name { get; set; }

        public Fruit(string name, string countryOfOrigin, double price, double weight, Promotion discount)
        {
            this.price = price;
            this.weight = weight;
            this.countryOfOrigin = countryOfOrigin;
            this.name = name;
            this.discount = discount;

            this.priceAfterPromotion = Ratio();
        }

        protected override string Ratio()
        {
            return base.Ratio();
        }
    }
}
