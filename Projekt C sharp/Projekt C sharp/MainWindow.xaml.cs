﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekt_C_sharp
{
    public partial class MainWindow : Window
    {
        public List<Fruit> listOfFruits { get; set; }
        public List<Drink> listOfDrinks { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            this.listOfFruits = new List<Fruit>();
            this.listOfFruits.Add(new Fruit("Banan", "Ekwador", 20, 1, Promotion.Brak));
            this.listOfFruits.Add(new Fruit("Pomarańcza", "Hiszpania", 10, 1, Promotion.Okazja));
            this.listOfFruits.Add(new Fruit("Jabłko", "Polska", 50, 1, Promotion.SuperOkazja));

            this.listOfFruits.Sort();

            this.listOfDrinks = new List<Drink>();
            this.listOfDrinks.Add(new Drink("Muszyna", IsFizzy.Tak, 2.5, 1.5, Promotion.Brak));
            this.listOfDrinks.Add(new Drink("Hortex", IsFizzy.Nie, 6.3, 2, Promotion.Brak));
            this.listOfDrinks.Add(new Drink("Frugo", IsFizzy.Nie, 1.5, 0.3, Promotion.Okazja));

            this.listOfDrinks.Sort();
        }


    }
}
