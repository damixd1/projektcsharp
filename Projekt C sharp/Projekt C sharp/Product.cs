﻿using System;

namespace Projekt_C_sharp
{
    public enum Promotion
    {
        Brak = 1,
        Okazja = 3,
        SuperOkazja = 7,
    };

    public class Product : IComparable<Product>
    {
        public double price { get; set; }
        public double weight { get; set; }
        public string priceAfterPromotion { get; set; }
        public Promotion discount { get; set; }

        protected virtual string Ratio()
        {
            string ratio;

            try
            {
                if (price / (int)discount < price)
                {
                    ratio = discount.ToString() + " " + Math.Round(((price / (int)discount) / weight), 2) + " za Kg";
                }
                else
                {
                    ratio = "Brak promocji";
                }
            }
            catch (DivideByZeroException e)
            {
                ratio = "Błąd";
            }

            return ratio;
        }

        public int CompareTo(Product other)
        {
            return this.price.CompareTo(other.price);
        }
    }
}
